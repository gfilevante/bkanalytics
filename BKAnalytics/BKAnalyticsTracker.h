//
//  BKAnalyticsTracker.h
//  BKAnalyticsTracker
//


#import <Foundation/Foundation.h>

extern NSString* const ADB_ACTION_NAME;
extern NSString* const ADB_ACTION_LABEL;
extern NSString* const ADB_ACTION_VALUE;
extern NSString *const ADB_VISITANTE_HASH;
extern NSString *const ADB_VISITANTE_SEGMENTO;


@interface BKAnalyticsTracker : NSObject

+ (void)trackViewWithController:(UIViewController *)controller;

+ (void)trackViewWithController:(UIViewController *)controller
                 withIdentifier:(NSString *)identifier;

+ (void)trackViewWithController:(UIViewController *)controller
                 withIdentifier:(NSString *)identifier
                       withHash:(NSString *) hash
                    withSegment:(NSString *) segmento;

+ (void)trackActionWithCategory:(NSString *)category
                 withActionName:(NSString *)actionName
                withActionLabel:(NSString *)actionLabel
                     withParams:(NSDictionary *)params;

+ (void)trackActionWithCategory:(NSString *)category
                 withActionName:(NSString *)actionName
                withActionLabel:(NSString *)actionLabel
                withActionValue:(NSNumber *) actionValue;

+ (void)trackActionWithCategory:(NSString *)category
                 withActionName:(NSString *)actionName
                withActionLabel:(NSString *)actionLabel
                withActionValue:(NSNumber *) actionValue
                       withHash: (NSString *) hash
                   withSegmento: (NSString *) segmento;

- (instancetype) init
__attribute__((unavailable("[-init] is not allowed")));

@end
