//
//  BKAnalyticsTracker.m
//  BKAnalyticsTracker
//
//

#import "BKAnalyticsTracker.h"
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAIFields.h>
#import <AdobeMobileSDK/ADBMobile.h>


static NSArray* Views = nil;
static NSString* const kResourceName = @"BKAnalytics-Tracker";

NSString* const ADB_ACTION_NAME = @"Action.Name";
NSString* const ADB_ACTION_LABEL = @"Action.Label";
NSString* const ADB_ACTION_VALUE = @"Action.Value";
NSString* const ADB_VISITANTE_HASH = @"Visitante.Hash";
NSString* const ADB_VISITANTE_SEGMENTO = @"Visitante.Segmento";
static NSString* const ADB_ACTION_FECHA = @"fecha_app";
static NSString* const ADB_ACTION_HORA = @"hora_app";


@interface BKAnalyticsTracker ()

+ (NSString *)viewNameForController:(UIViewController *)controller;
@end



@implementation BKAnalyticsTracker

#pragma mark -
#pragma mark - NSObject

+ (void)load
{
#ifdef NZDEBUG
    NSLog(@"%s", __PRETTY_FUNCTION__);
#endif
    
    [super load];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:kResourceName ofType:@"plist"];
    
    if (path && [[NSFileManager defaultManager] fileExistsAtPath:path]) {
        Views = [[NSArray alloc] initWithContentsOfFile:path];
    }
    
    if (Views == nil) {
        NSLog(@"%s \"%@.plist\" not found or not configurated. \
              Check if the file has been added to your project and add 'class_name' keys and 'view_name' values to array.",
              __PRETTY_FUNCTION__, kResourceName);
    }
}

#pragma mark -
#pragma mark - Public methods

+ (void)trackViewWithController:(UIViewController *)controller
{
    [self trackViewWithController:controller withIdentifier:nil];
}

+ (void)trackViewWithController:(UIViewController *)controller
                 withIdentifier:(NSString *)identifier
{
    [self trackViewWithController:controller withIdentifier:identifier withHash:@"anonimo" withSegment:@"anonimo"];
}

+ (void)trackViewWithController:(UIViewController *)controller
                 withIdentifier:(NSString *)identifier
                       withHash:(NSString *) hash
                    withSegment:(NSString *) segmento {
    NSString *viewName = [self viewNameForController:controller];
    
    if (viewName) {
        NSMutableString *string = [[NSMutableString alloc] initWithString:@""];
        [string appendString:viewName];
        
        if (identifier) {
            [string appendFormat:@"/%@", identifier];
        }
        
        // Google tracker
        id<GAITracker> googleTracker = [[GAI sharedInstance] defaultTracker];
        [googleTracker set:kGAIScreenName value:string];
        [googleTracker send:[[GAIDictionaryBuilder createAppView] build]];
        NSDictionary *contextData = nil;
        
        if (hash!=nil && segmento!=nil) {
            contextData = [NSDictionary dictionaryWithObjectsAndKeys:
                           hash, ADB_VISITANTE_HASH,
                           segmento, ADB_VISITANTE_SEGMENTO,
                           nil];
        }
        // Adobe tracker
        [ADBMobile trackState:string data:contextData];
    } else {
#ifdef NZDEBUG
        NSLog(@"%s nil tracker name for controller: %@",
              __PRETTY_FUNCTION__, NSStringFromClass([controller class]));
#endif
    }
}


+ (void)trackActionWithCategory:(NSString *)category
                 withActionName:(NSString *)actionName
                withActionLabel:(NSString *)actionLabel
                     withParams:(NSDictionary *)params {
    if (category) {
        // Google tracker
        [[[GAI sharedInstance] defaultTracker]
         send:[[GAIDictionaryBuilder
                createEventWithCategory:category
                action:actionName
                label:actionLabel
                value:nil] build]];
        
        // Adobe tracker
        NSMutableDictionary *contextData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                            actionName, ADB_ACTION_NAME,
                                            actionLabel, ADB_ACTION_LABEL,
                                            nil];
        [contextData setValuesForKeysWithDictionary:[self dateTimeContext]];
        if (params!=nil) {
            [contextData setValuesForKeysWithDictionary:params];
        }
        [ADBMobile trackAction:category data:contextData];
    }
}

+ (void)trackActionWithCategory:(NSString *)category
                 withActionName:(NSString *)actionName
                withActionLabel:(NSString *)actionLabel
                withActionValue:(NSNumber *) actionValue
{
    if (category) {
        // Google tracker
        [[[GAI sharedInstance] defaultTracker]
         send:[[GAIDictionaryBuilder
                createEventWithCategory:category
                action:actionName
                label:actionLabel
                value:actionValue] build]];
        
        
        
        // Adobe tracker
        NSDictionary *contextData = [NSDictionary dictionaryWithObjectsAndKeys:
                                     actionName, ADB_ACTION_NAME,
                                     actionLabel, ADB_ACTION_LABEL,
                                     actionValue, ADB_ACTION_VALUE,
                                     nil];
        
        [ADBMobile trackAction:category data:contextData];
        
        
    }
}



+ (void)trackActionWithCategory:(NSString *)category
                 withActionName:(NSString *)actionName
                withActionLabel:(NSString *)actionLabel
                withActionValue:(NSNumber *) actionValue
                       withHash: (NSString *) hash
                   withSegmento: (NSString *) segmento
{
    if (category) {
        // Google tracker
        [[[GAI sharedInstance] defaultTracker]
         send:[[GAIDictionaryBuilder
                createEventWithCategory:category
                action:actionName
                label:actionLabel
                value:actionValue] build]];
        
        
        
        // Adobe tracker
        NSDictionary *contextData = [NSDictionary dictionaryWithObjectsAndKeys:
                                     actionName, ADB_ACTION_NAME,
                                     actionLabel, ADB_ACTION_LABEL,
                                     hash, ADB_VISITANTE_HASH,
                                     segmento, ADB_VISITANTE_SEGMENTO,
                                     actionValue, ADB_ACTION_VALUE,
                                     nil];
        
        [ADBMobile trackAction:category data:contextData];
        
        
    }
}



#pragma mark -
#pragma mark - Private methods

+ (NSString *)viewNameForController:(UIViewController *)controller
{
    NSString *controllerName = NSStringFromClass([controller class]);
    NSString *viewName = nil;
    
    if (Views) {
        for (NSDictionary *dictionary in Views) {
            if (dictionary) {
                NSString *className = [dictionary objectForKey:@"class_name"];
                
                if (className && [className isEqualToString:controllerName]) {
                    viewName = [dictionary objectForKey:@"view_name"];
                    break;
                }
            }
        }
    }
    
    return viewName;
}

+(NSDictionary *) dateTimeContext {
    NSDate *currentDate = [[NSDate alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"ddMMyyyy'"];
    NSString *utcDateString = [dateFormatter stringFromDate:currentDate];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *utcTimeString = [dateFormatter stringFromDate:currentDate];
    NSMutableDictionary *contextData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        utcDateString, ADB_ACTION_FECHA,
                                        utcTimeString, ADB_ACTION_HORA,
                                        nil];
    return contextData;
}

@end
