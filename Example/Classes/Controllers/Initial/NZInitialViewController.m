//
//  NZInitialViewController.m
//  NZGoogleAnalytics
//
//  Created by Bruno Tortato Furtado on 14/12/13.
//  Copyright (c) 2013 No Zebra Network. All rights reserved.
//

#import "NZInitialViewController.h"
#import "BKAnalyticsTracker.h"

@implementation NZInitialViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [BKAnalyticsTracker trackViewWithController:self];
}

@end