Pod::Spec.new do |s|
  s.name = 'BKAnalytics'
  s.version = '0.0.7'
  s.license = { :type => 'MIT', :file => 'LICENSE' }
  s.summary = 'Quickly and easily integration with Google Analytics and Adobe Analytics API for iOS.'
  s.homepage = 'https://bitbucket.org/gfilevante/bkanalytics.git'
  
  s.requires_arc = true
  s.platform = :ios
  s.ios.deployment_target = '8.0'
  
  s.authors = { 'Fidel Cornelis' => 'fpcornelis@gfi.es' }
  
  s.dependency 'GoogleAnalytics', '3.12.0'
  s.dependency 'NZBundle', '0.0.4'
  s.dependency 'AdobeMobileSDK', '4.4.0'
  s.source_files = 'BKAnalytics/*.{h,m}'
  s.source = { :git => 'https://bitbucket.org/gfilevante/bkanalytics.git', :tag => "#{s.version}" }  
end